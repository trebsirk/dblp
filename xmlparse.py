from lxml import etree
import re, collections, math, sys, time
import operator, pickle, itertools

def fast_iter(context, func):
    # http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    # Author: Liza Daly
    for event, elem in context:
    	print elem
        func(elem)
        elem.clear()
        while elem.getprevious() is not None:
            del elem.getparent()[0]
    del context

def process_element(elem):
    print elem.xpath( 'description/text( )' )

CATEGORIES = set(['article', 'inproceedings', 'proceedings', 'book', 'incollection', 'phdthesis', "mastersthesis", "www"])
DATA_ITEMS = ["title", "booktitle", "year", "category"]

def clear_element(element):
	element.clear()
	while element.getprevious() is not None:
		del element.getparent()[0]

def words(text): return re.findall('\w+', text.lower())

def fast_iter2(context, target_year, dict):
	# Available elements are:   article|inproceedings|proceedings|book|incollection|phdthesis|mastersthesis|www
	# Available tags are:       author|editor|title|booktitle|pages|year|address|journal|volume|number|month|url|ee|cdrom|cite|publisher|note|crossref|isbn|series|school|chapter
	for event, element in context:
		if element.tag in CATEGORIES:
			year = element.find('year')
			if year is not None:
				year = year.text
				if year == target_year:
					authors = [author.text for author in element.findall('author')]
					for au in authors:
						if au not in dict:
							dict[au] = {}
					if len(authors) > 1:
						for a1 in authors:
							for a2 in authors:
								if a1 != a2:
									if a1 in dict:
										if 'coauthors' in dict[a1]:
											if a2 not in dict[a1]['coauthors']:
												dict[a1]['coauthors'][a2] = 1
										else:
											dict[a1]['coauthors'] = {}
											dict[a1]['coauthors'][a2] = 1
									else:
										dict[a1] = {}
										dict[a1]['coauthors'] = {}
										dict[a1]['coauthors'][a2] = 1
					title = element.find('title').text
					if title is not None:
						title = words(title)
						if title is not []:
							for au in authors:
								if 'words' in dict[au]:
									dict[au]['words'].append(title)
								else:	
									dict[au]['words'] = [title]

			clear_element(element)
			
first_year = int(sys.argv[1])
for yr in range(first_year, first_year+int(sys.argv[2])):
	filename = str(yr) + '.p'
	print 'processing for ' + str(yr) + ': ' + filename
	dict = {}
	context = etree.iterparse('dblp.xml')
	fast_iter2(context, str(yr), dict)
	pickle.dump(dict, open(filename, 'wb'))
